# Changelog

All notable changes to this project will be documented in this file

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html)

## [Unreleased]

* `newsboat` Vi bindings and more feed URLs
* `mpv.sh` strips YouTube playlist information from the URL, allowing `youtube-dl` to parse it correctly
* `torbrowser-launcher` replaces `firefox` on `xinit`
* ... and many more features planned!

## [2.2.0] - 2021-09-11

### Added

* `newsboat` to displays RSS feeds and notifications
* `apt-listchanges` to compare package versions when upgrading
* Mouse acceleration fix
* CJK (Chinese, Japanese, Korean) font support to `polybar`
* `bspwmrc.local` sourcing for additional window manager rules
* `unattended-upgrades` for security and backports upgrades
* `gnome-keyring` for Nextcloud and Tutanota secrets
* Swappiness configuration to avoid swapping early
* `disable_ipv6` kernel parameters
* Backports sources list
* `install.sh` as a naive installer
* `piper` window manager rule
* `Xwrapper.config` for rootless Xorg with non-free Nvidia drivers
* `gtk-3.0` for newer (non-deprecated) GTK applications
* `qt5-style-plugins` for Qt applications to inherit the GTK theme
* `gstreamer1.0-alsa` as a hard dependency for `mopidy`
* `apt-file` for `list` and `search` aliases
* `trash-cli` for `trash-put` alias
* `fd-find`, `findutils`, `grep` and `ripgrep` to provide binaries for `find` and `grep` functions
* `mpd` `polybar` module to display song artist and title
* `tlp` for ThinkPad power management
* `/opt` binaries to `$PATH`
* More `git` aliases
* Freedesktop sounds played after `amixer` volume changes
* ThinkPad Trackpoint rules
* Tutanota desktop file
* `redshift` configuration
* `nextcloud-desktop` for cloud synchronisation
* `sa()` to search aliases
* `etc/apt/preferences.d/99debian-backports-dotfiles` to upgrade stale packages
* Keybind to save screenshots to ~/Pictures

### Changed

* `torbrowser-launcher` floats as an anti-fingerprinting measure
* `apt remove` implicitly invokes `--purge`
* `apt install` and `apt remove` automatically remove unused dependencies
* Programatically evaluate `polybar` `temperature` input
* `mpDris2` does not notify on track change
* Use GNU long options when invoking `xclip`, `bspc`, `pgrep` and `id` in scripts
* Configure `rofi` using `Xresources` syntax
* Redirect contents of `share/dotfiles/MOPIDY` to `STDIN`
* Redirect commands followed by OR to `STDERR`
* `grep()` and `find()` search hidden and ignored files
* `cp` prompts before overwriting
* Volume adjusted in 1.5dB increments
* `git mv` doesn't skip files which aren't version-controlled
* `git rm` unstages version-controlled files
* `find()` accepts more than two arguments
* `sh/` is a subdirectory of `share/dotfiles/`
* `nvim` ignores filename case

### Removed

* Duplicate packages
* Superfluous D-Bus environment variables and session bus
* Installation instructions in lieu of an installer script
* `firejail` due to [security concerns](https://cve.mitre.org/cgi-bin/cvekey.cgi?keyword=firejail)
* `redshift-gtk` as an optional dependency
* Mistakenly tracked `config/X11/Xresources.d/fonts` symbolic link

### Fixed

* Wrap `apt-get install` command with `xargs` to ensure shell redirection works
* Write binary-converted `mopidy.gpg` to `/usr/share/keyrings` and verify packages using that file
* Pin the `tmux` Backports package to ensure `tmux.conf` loads from `$XDG_CONFIG_HOME/tmux`
* Look up running `polybar` processes before sending `SIGTERM`
* Invoke `chown` and `chmod` as `sudo` when creating `$auth_file`
* Source `share/dotfiles/sh/profile` on first install
* Missing copyright notices
* `return` from the `config/X11/xinitrc.d/nonfree` if condition

## [2.1.1] - 2021-05-10

### Changed

* `then` statements are placed on a newline
* `bspc_sxhkdrc` is started by `xinitrc`

### Fixed

* Retab all files
    - `nvim` uses 4 spaces in a tab and converts tabs to spaces 
*  `echo $SHELL` `stdout` is redirected when sourcing `home/.zshenv` or `home/.bash_profile`

## [2.1.0] - 2021-05-08

### Added

* Check for non-free component in `/etc/apt/sources.list` before executing `config/X11/xinitrc.d/nonfree`
* `dotfiles-sh` merge
    - 'Shell-agnostic aliases, functions and profile'
    - 'Add bash and zsh-specific configuration'
* Required packages:
    - `imagemagick-6.q16`
* PCM volume adjustment hotkey
* Persistent `nvim` undo/redo

### Changed

* Tab size reduced to 4
* Replace `maim` with `import`

### Removed

* Obsolete packages:
    - `maim`

### Fixed

* Make additional directories required by `nvim` and `zsh`

## [2.0.0] - 2021-05-05

### Added

* Command resolution for optional programs
* Warnings against the freedom of `libspotify`
* Hardened configuration for `firejail firefox`
* Required packages:
    - `cpp`
    - `firefox-esr`
    - `firejail-profiles`
    - `firejail`
    - `gmpc`
    - `hsetroot`
    - `keepassxc`
    - `mpdris`
    - `mpv`
    - `neovim`
    - `xfonts-terminus`
    - `xinit`
    - `xserver-xorg-core`
    - `xserver-xorg-input-libinput`
    - `youtube-dl`
    - `zsh`
* Pinned third-party repository to install Mopidy packages from
* Instructions to create a restrictive `spotify_authentication.conf`
* File to [prevent installation of Recommends packages](https://wiki.debian.org/ReduceDebian#Reconfigure_apt_so_that_it_does_not_install_additional_packages)
* [Apt pinning](https://wiki.debian.org/DontBreakDebian#Don.27t_make_a_FrankenDebian) when installing packages from the Mopidy repository
* Configuration for `mopidy-mpd` and `mopidy-spotify`
* Pattern to ignore untracked `TODO*` files
* Battery percentage in the `tmux` status bar
* More `polybar` statistics
    - Battery capacity remaining (for up to two batteries)
    - Average CPU load
    - Memory used
    - Temperature
* Terminus fonts for `config/X11/Xresources`
* GNU Head tiling wallpaper
* `zoom`, `xfreerdp` and `teams` desktop rules
* Installation instructions for `equilux-theme`, `equilux-firefox` and `papirus-folders`
* Date string when clicking on the time in `polybar`
* Customisation for `readline` programs
* Setting to prevent programs from stealing focus
* Additional actions when locking the screen
    - Pause `dunst` notifications
    - Disable virtual console switching
    - Disable SysRq key
* Terminal scrollback jumping prevention
* Multimedia key volume adjustment

### Changed

* Merge `xinit/xprofile` into `config/X11/xinitrc`
* Include 2021 release in copyright notices
* Stow `lib/dotfiles/` scripts to `~/.local/lib/`
    - See `file-hierarchy(7)` __HOME DIRECTORY__
* Replace `"$HOME"` expansion with `~`
* Use a C preprocessor to source Noto or Terminus fonts from `config/X11/Xresources.d/`
* Move package directories to top-level directories
* Replace `spotify` with `gmpc`
* Replace `~/.dotfiles` with `$dotfiles_dir` when cloning, installing packages and stowing
* Invoke Tutanota as `tutanota-desktop-linux.AppImage`
* Use different desktop [icons](https://fontawesome.com/v4.7.0/cheatsheet/):
    4. `fa-video-camera`
    5. `fa-desktop`
    8. `fa-music`
    9. `fa-envelope`
* Replace `~/.config/` and `~/.local/share/` with `$XDG_CONFIG_HOME` and `$XDG_DATA_HOME`
* Implicitly invoke `stow` and `xrdb` options
* Track remote repositories when updating submodules
* Configure `tmux` and `tpm` to respect the XDG Base Directory Specification
* Use default intervals for `polybar` modules
* Invoke `lib/dotfiles/mpv.sh` using the 'super + m' hotkey
* Re-arrange desktops:
    3. `zotero`, `soffice`
    6. `mpv`
    7. `lutris`, `steam`, `ckb-next`
    8. `gmpc`
    9. `tutanota-desktop-linux.AppImage`
* Tile Tor Browser and Zathura windows
* Use `#! /usr/bin/env dash` shebangs in `lib/dotfiles/` scripts
* After watching a video in `mpv,` desktop focus is changed only if there are no concurrent `mpv` processes
* Recommends packages are not considered when installing packages
* Screenshots are saved to `/tmp`
* `papirus-folders` does not require an [unsafe `sh` pipe](https://www.seancassidy.me/dont-pipe-to-your-shell.html) when installing
* `wait` replaces `pgrep` when waiting for the `killall polybar` job to conclude
* Polybar modules have been re-positioned:
    - Left: Tray
    - Centre: Workspaces
    - Right: Date/Time
* Use `nvim` as a drop-in replacement for `vim`
* Open three `urxvtc` windows at startup
* Use the default `dunst` separator

### Removed

* `stow` target directories
* `urxvt.patch` in lieu of `config/X11/Xresources.d/*_fonts`
* `spotify` (and corresponding files\*) in lieu of `mopidy-spotify`
    - \* `bspwm/spotifywm`, `bspwm/spotify.sh`, `firejail/spotify.local`
* `{lx}appearance/` in lieu of `README.md` and `config/gtk-2.0`
* `material.rasi` in lieu of `Paper.rasi`
* `$SYSTEM\_RESOURCES`, switchable X sessions and runcoms from `config/X11/xinitrc`
* `ckb-next.patch`, `nm-applet.patch`, `polybar.patch`, `redshift-gtk.patch`, `sxhkd.patch` and `tmux.patch`, following mainline incorporation
* Obsolete packages:
    - `coreutils`
    - `feh`
    - `groff-base`
    - `i3-wm`
    - `imagemagick`
    - `less`
    - `lxappearance`
    - `patch`
    - `sound-theme-freedesktop`
    - `unzip`
    - `vim-gtk`
    - `wmctrl`
* Ripcord from startup and window manager rules
* Freedesktop sounds played after `amixer` volume changes
* `feh` and `~/.fehbg` in lieu of `hsetroot`
* `config/gtk-2.0/gtkrc` `include` statement
* `lessfilter` Markdown rendering
* `config/X11/Xresources` `secondaryWheel` option
    - Not supported in unpatched versions of `rxvt-unicode`
* `.stow-local-ignore` files, following directory changes
* Files related to the i3 window manager and Void Linux distribution
    - See 42cf891
* Spotify Flatpak and `wmctrl` workaround
* `sxhkdrc` shortcuts in lieu of `rofi`
* `notify-send` on `sxhkd` reload
* `README.md` instructions to reload configuration files, patch files and clone using `ssh`

### Fixed

* Removed `--quality` (`-m`) option from `maim` commands
    - `.png` output is lossless
* Invoke `xdg-user-dirs-update` to ensure that the default XDG Base Directory Specification directories are present
* Added `xclip`, `fuse`, `psmisc` as required packages
    - Required by `lib/dotfiles/mpv.sh`, `tutanota-desktop-linux.AppImage`, killall` respectively
* Launch `tutanota-desktop-linux.AppImage` with the `--no-sandbox` option
* Added copyright notices to configuration files and scripts
* Added `dbus-update-activation-environment` to `config/X11/xinitrc`
    - Prevents D-Bus timeout when starting GTK applications
    - See `dbus-update-activation-environment(1)` __EXAMPLES__
* Added programs invoked by `config/X11/xinitrc` to `share/dotfiles/DEBIAN`

## [1.0.0] - 2020-06-18

### Added

* XF86Audio commands may now be issued with the Super and Arrow keys
    - This design is inspired by the ThinkPad media control (fn) keys
* `polybar` now contains an `i3` module, derived from the `bspwm` module
* A naive implementation of `dunst` is available
    * Notification messages will be displayed via `dunst`
    * `sxhkd/sxhkdrc` now sends a notification when it has reloaded itself
* `spotifywm` has been added as a submodule
    - The shared library is expected to reside in `bspwm/spotifywm/`, and is called from `bspwm/spotify.sh`
    - Compiled files are not tracked
* `bspwm/spotify.sh` handles launching and moving the Spotify window
    - Usable on the binary and Flatpak versions
* `i3/spotify.sh` handles launching the Spotify window
    - Usable on the binary and Flatpak versions
* `tutanota-desktop` is moved to desktop 3 on creation

### Changed

* `bindsym` commands in `i3/config` have been transposed to `sxhkd/i3-msg_sxhkdrc`
* `polybar/launch.sh` uses `dash`, replacing `bash`
    - `dash` is employed throughout `dotfiles`' scripts as it ['focuses on speed and compatibility with standards'](https://wiki.debian.org/Shell)
* `bspwm/bspwmrc` and `i3/config` contain specific startup programs, which are not suitable for `xinit/xprofile`
* Moving a floating window in `bspwm` now requires the Control key to be pressed, in addition to the Super and Arrow keys
* `amixer` commands are now generic, referring to the Master control of the device in use
* Killing a window, or ending an X session, now requires that the Shift key be pressed alongside the respective hotkey (k, q)
    - These are termed 'destructive actions', requiring the Shift key is subsequently termed a 'safety'
* Window resizing in `bspwm` has been consolidated into a single hotkey sequence
* The value windows are resized by in `bspwm` has been reduced to 120 (from 180)
* `playerctl pause` is executed before locking the screen with `xtrlock`
* The `README`'s introduction has been transformed into a Features list
* `polybar/config` uses new icons for `ws-icon-0` (code) and `ws-icon-9` (database)
* The licence header in `README` has moved to the end of the file
* The size of borders for `i3` windows has been increased to 2 (from 1)

### Removed

* Submodules, files, configuration text and packages related to `i3blocks` and `i3lock` have been removed
* `patches/keyboard/` and `patches/void/` have been removed, and their patches mainlined or obsoleted
* Commit history has been squelched
    - Anti-patterns and poor practices have been absolved

### Fixed

* Patches have been updated and now apply properly
    - Historically, patches and their target files have been subject to rewrites, necessitating reapplication of patches between releases
    - Should this trend continue, breaking changes will feature in future logs
