#! /usr/bin/env dash

# dotfiles  Configuration files and scripts for Debian GNU/Linux systems
# Copyright (C) 2020, 2021  James Davidson
#
# This file is part of dotfiles.
#
# dotfiles is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# dotfiles is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Exit script when a command fails (returns a non-zero exit code)
set -o errexit

# Evaluate if the user can obtain superuser privileges
if ! sudo --validate
then
    printf '%b' "\\nAccess to the sudo or wheel group is required!\\n"
    exit 1
fi

# Read path from standard input
read -p 'Enter absolute path to dotfiles directory: ' -r DOTFILES_DIR

# Clone repository and submodules
git clone --recurse-submodules https://gitlab.com/jdxyz/dotfiles.git \
    "$DOTFILES_DIR"

# Install dependencies
sudo apt-get update && xargs sudo apt-get --no-install-recommends --assume-yes \
    install < "$DOTFILES_DIR"/share/dotfiles/DEBIAN

# Ignore SC1090 when sourcing
# shellcheck source=/dev/null

# Source environment variables
. "$DOTFILES_DIR"/share/dotfiles/sh/profile

# Make XDG Base Directory Specification directories according to user-dirs.dir
xdg-user-dirs-update

# If Stow installs to a target directory which does not already exist,
# other files placed in that directory are placed under version control

# Make target directories and other directories required by programs
mkdir --parents "$XDG_CACHE_HOME"/zsh \
    "$XDG_CONFIG_HOME"/bspwm \
    "$XDG_CONFIG_HOME"/dunst "$XDG_CONFIG_HOME/gtk-2.0" \
    "$XDG_CONFIG_HOME/gtk-3.0" "$XDG_CONFIG_HOME"/mpDris2 \
    "$XDG_CONFIG_HOME"/nvim "$XDG_CONFIG_HOME"/polybar \
    "$XDG_CONFIG_HOME"/readline "$XDG_CONFIG_HOME"/rofi \
    "$XDG_CONFIG_HOME"/sxhkd "$XDG_CONFIG_HOME"/tmux \
    "$XDG_CONFIG_HOME"/X11/Xresources.d "$XDG_CONFIG_HOME"/zsh \
    "$XDG_DATA_HOME"/applications "$XDG_DATA_HOME"/dotfiles \
    "$XDG_DATA_HOME"/nvim/undo "$XDG_DATA_HOME"/zsh \
    "$HOME"/.local/lib/dotfiles

# Trash conflicting files
trash-put "$HOME"/.bashrc
sudo trash-put /etc/sysctl.conf /etc/apt/apt.conf.d/50unattended-upgrades \
    /etc/X11/Xwrapper.config

# Install to user-owned directories
stow --dir="$DOTFILES_DIR" --target="$XDG_CONFIG_HOME" config
stow --dir="$DOTFILES_DIR" --target="$XDG_DATA_HOME" share
stow --dir="$DOTFILES_DIR" --target="$HOME" home
stow --dir="$DOTFILES_DIR" --target="$HOME"/.local/lib lib

# Install to root-owned directories
sudo stow --dir="$DOTFILES_DIR" --target=/etc etc

# polybar is only available from buster-backports
sudo apt-get update && xargs sudo apt-get --assume-yes install < \
    "$DOTFILES_DIR"/share/dotfiles/DEBIAN-BACKPORTS

# Use laptop fonts (Terminus) if a laptop is detected, else use default fonts
# (Noto Mono)
if laptop-detect
then
    ln --symbolic "$DOTFILES_DIR"/config/X11/Xresources.d/laptop_fonts \
        "$XDG_CONFIG_HOME"/X11/Xresources.d/fonts
else
    ln --symbolic "$DOTFILES_DIR"/config/X11/Xresources.d/default_fonts \
        "$XDG_CONFIG_HOME"/X11/Xresources.d/fonts
fi

# Download the Equilux GTK theme
wget --directory-prefix=/tmp -- \
    'https://github.com/ddnexus/equilux-theme/archive/equilux-v20181029.tar.gz'
# Extract from the archive
tar --extract --file=/tmp/equilux-v20181029.tar.gz --directory=/tmp
# Create /usr/share/themes if it does not exist
sudo mkdir --parents /usr/share/themes
# Install Equilux and Equilux-compact to /usr/share/themes
sudo /tmp/equilux-theme-equilux-v20181029/install.sh

# Download Papirus Folders script
wget --directory-prefix=/tmp -- 'https://git.io/papirus-folders-install'
# Install papirus-folders to /usr/bin
dash /tmp/papirus-folders-install
# Set folder colour to grey (visible in graphical file managers)
papirus-folders --color grey --theme Papirus-Dark
