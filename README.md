# Installation

* Create a `dotfiles` directory which can be read and written to by the user
* Download [install.sh](install.sh) to the `dotfiles` directory

    ```
    sudo apt-get --no-install-recommends install ca-certificates findutils git wget
    chmod +x install.sh
    ./install.sh
    ```

## Appearance

* [Equilux-Firefox](https://github.com/cj-sv/equilux-firefox) for Firefox
    - See `about:profiles` for the Root Directory

    ```
    profile_dir=/path/to/profile/root/directory
    mkdir ~/.mozilla/firefox/$profile_dir/chrome
    wget -P ~/.mozilla/firefox/$profile_dir/chrome \
        https://raw.githubusercontent.com/cj-sv/equilux-firefox/master/userChrome.css
    wget -P ~/.mozilla/firefox/$profile_dir/chrome \
        https://raw.githubusercontent.com/cj-sv/equilux-firefox/master/userContent.css 
        
    ```

## Spotify (non-free)

`mopidy-spotify` recursively depends on `libspotify12`, which is deprecated and non-free

* [Install Mopidy's GPG key](https://docs.mopidy.com/en/latest/installation/debian/#install-from-apt-mopidy-com)
* [Convert key to binary](https://wiki.debian.org/DebianRepository/UseThirdParty#OpenPGP_key__handling)
* Install Mopidy and dependencies

    ```
    sudo wget -P /usr/share/keyrings https://apt.mopidy.com/mopidy.gpg

	gpg --import /usr/share/keyrings/mopidy.gpg && \
        gpg --export 9E36464A7C030945EEB7632878FD980E271D2943 | \
		sudo tee /usr/share/keyrings/mopidy.gpg

    sudo apt-get update && xargs sudo apt-get install \
        < "$DOTFILES_DIR"/share/dotfiles/MOPIDY
    ```

* Use a `.conf` file for `mopidy-spotify` authentication

    Enter username and password, then [client identifier and secret](https://mopidy.com/ext/spotify/)

    ```
    auth_file='/usr/share/mopidy/conf.d/spotify_authentication.conf'
    sudo touch $auth_file
    sudo chown mopidy:root $auth_file && sudo chmod 600 $auth_file

    printf '%b' "username = \\npassword =\\n" | sudo tee $auth_file
    sudoedit $auth_file
    ```

# Updating

```
git pull master && git submodule update --progress --init --remote --recursive
```

# [Licence](COPYING)

    dotfiles  Configuration files and scripts for Debian GNU/Linux systems
    Copyright (C) 2020, 2021  James Davidson

    This file is part of dotfiles.

    dotfiles is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3.

    dotfiles is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

