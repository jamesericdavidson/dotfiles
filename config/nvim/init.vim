" dotfiles  Configuration files and scripts for Debian GNU/Linux systems
" Copyright (C) 2020, 2021  James Davidson
"
" This file is part of dotfiles.
"
" dotfiles is free software: you can redistribute it and/or modify
" it under the terms of the GNU General Public License as published by
" the Free Software Foundation, version 3.
"
" dotfiles is distributed in the hope that it will be useful,
" but WITHOUT ANY WARRANTY; without even the implied warranty of
" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
" GNU General Public License for more details.
"
" You should have received a copy of the GNU General Public License
" along with this program.  If not, see <https://www.gnu.org/licenses/>.

autocmd! User GoyoEnter Limelight
autocmd! User GoyoLeave Limelight!
let g:limelight_conceal_ctermfg = 'DarkGrey'

colorscheme desert
let mapleader=','

" :help options
set cursorline ignorecase lazyredraw number relativenumber showmatch smartcase
set undofile wildignorecase

" :help :map-commands
nnoremap ; :
nnoremap : <nop>

imap jj <ESC>

nmap <C-h> <C-w>h
nmap <C-j> <C-w>j
nmap <C-k> <C-w>k
nmap <C-l> <C-w>l

nmap <leader>v <C-w>v<C-w>l

nmap <leader>O O<Esc>j
nmap <leader>o o<Esc>k

nmap <silent><leader><C-l> ;nohlsearch<CR>

nmap <silent><leader>sc ;setlocal spell! spelllang=en_gb<CR>

nmap <silent><leader>sv ;source $MYVIMRC<CR>

vmap <leader>cx "+y<CR>

" :help tabstop
set tabstop=4 shiftwidth=4 expandtab
