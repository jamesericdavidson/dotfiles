# dotfiles  Configuration files and scripts for Debian GNU/Linux systems
# Copyright (C) 2020, 2021  James Davidson
#
# This file is part of dotfiles.
#
# dotfiles is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# dotfiles is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

source "$XDG_DATA_HOME"/dotfiles/sh/aliases
source "$XDG_DATA_HOME"/dotfiles/sh/functions

alias \
    -- -='cd -' \
    -- ~='~ && clear' \
    reload="source $XDG_CONFIG_HOME/zsh/.zshrc && source $HOME/.zshenv" \
    whence='whence -vfa' \
    where=whence

# Parameters -- zshall(1)
HISTSIZE=1000
KEYTIMEOUT=1
PS1='%F{blue}%1~ %#%f '
RPS1=\$vcs_info_msg_0_
SAVEHIST=1000

# Options -- zshoptions(1)
setopt AUTO_CD GLOB_DOTS HIST_IGNORE_ALL_DUPS PROMPT_SUBST SHARE_HISTORY
unsetopt BEEP

# Autoloading functions -- zshall(1)
autoload -U compinit vcs_info

# Use of compinit -- zshcompsys(1)
compinit -d "$XDG_CACHE_HOME"/zsh/zcompdump-"$ZSH_VERSION"

# Standard styles -- zshcompsys(1)

zstyle ':completion::complete:*' gain-privileges true
zstyle ':completion:*' rehash true
zstyle ':completion:*' menu select
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' \
    'r:|[._-]=* r:|=* l:|=*'
eval "$(dircolors --bourne-shell)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}

# Configuration -- zshcontrib(1)
zstyle ':vcs_info:git:*' formats '%F{yellow}%b%f'
zstyle ':vcs_info:git:*' actionformats '%F{yellow}%b (%a)%f'

# Quickstart -- zshcontrib(1)
zstyle ':vcs_info:*' enable git

# ZLE builtins -- zshzle(1)
bindkey "^[[A" history-search-backward
bindkey "^[[B" history-search-forward

# Hook functions -- zshmisc(1)
chpwd() ls
precmd() { vcs_info }
