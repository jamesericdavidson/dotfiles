# dotfiles  Configuration files and scripts for Debian GNU/Linux systems
# Copyright (C) 2020, 2021  James Davidson
#
# This file is part of dotfiles.
#
# dotfiles is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# dotfiles is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

[global]

# Define width, height, horizontal and vertical offsets of the
# notification window
geometry = "240x24-24+48"

# Define padding from content to window border
padding = 20
horizontal_padding = 20

# Define width of frame surrounding the notification window
frame_width = 2

# Define frame colour
frame_color = "#2d2d2d"

# Display notifications by ascending order of urgency
sort = false

# If user is idle longer than the threshold, don't time out
# notifications
idle_threshold = 120

# Define font and size
font = Noto Sans Display 12

# Handle Pango Markup
markup = full

# Define format of the message
format = "<b>%s</b>\n%b"

# Align notification text
alignment = center

# Show age of message, if it's older than the threshold
show_age_threshold = 60

# Wrap lines if they don't fit inside the geometry
word_wrap = true

# Don't show indicators for actions or URLs
show_indicators = false

# Command to run when opening a URL
browser = /usr/bin/firefox -new-tab

[shortcuts]

close = mod1+space
close_all = mod1+shift+space
history = mod1+grave
context = mod1+shift+period

[urgency_low]

background = "#929292"
foreground = "#2d2d2d"
timeout = 30

[urgency_normal]

background = "#929292"
foreground = "#2d2d2d"
timeout = 10

[urgency_critical]

background = "#929292"
foreground = "#2d2d2d"
timeout = 0
