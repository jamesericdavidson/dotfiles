#! /usr/bin/env dash

# dotfiles  Configuration files and scripts for Debian GNU/Linux systems
# Copyright (C) 2020, 2021  James Davidson
#
# This file is part of dotfiles.
#
# dotfiles is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# dotfiles is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Copy a video link (parseable by youtube-dl) and play it in mpv

# Print X clipboard buffer to STDOUT
# Execute mpv in the background
mpv --fullscreen "$(xclip -out)" &

# Focus the video desktop
bspc desktop --focus 6

if [ "$(playerctl status)" = 'Playing' ]
then
    playerctl pause
fi

# Wait for the background process (mpv) to return an exit code
wait $!

# If there are no mpv processes remaining...
if ! pgrep --euid "$(id --user --name)" --exact mpv
then
    # Focus the browser desktop
    bspc desktop --focus 2
fi
