#! /usr/bin/env dash

# dotfiles  Configuration files and scripts for Debian GNU/Linux systems
# Copyright (C) 2020, 2021  James Davidson
#
# This file is part of dotfiles.
#
# dotfiles is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# dotfiles is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

if pgrep --euid "$(id --user --name)" --exact polybar
then
    # Kill existing polybar processes
    killall --quiet polybar
    # Wait for the processes to die
    wait $!
fi

for i in /sys/class/hwmon/hwmon*/temp*_input
do
    if [ "$(cat "$(dirname "$i")"/name): $(cat "${i%_*}"_label 2>/dev/null || "basename ${i%_*}")" = "coretemp: Core 0" ]
    then
        export HWMON_PATH="$i"
    fi
done

# Launch Polybar
polybar bspwm &
