#! /usr/bin/env dash

# dotfiles  Configuration files and scripts for Debian GNU/Linux systems
# Copyright (C) 2020, 2021  James Davidson
#
# This file is part of dotfiles.
#
# dotfiles is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# dotfiles is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# The ability to restart bspwm with bspc was added after 0.9.5

VERSION="$(bspwm -v)"
readonly TARGET='0.9.6'

# If using an older version, bspwmrc should be called to handle
# instantiation of bspc configs and rules

if [ "$VERSION" \< "$TARGET" ]
then
    "$XDG_CONFIG_HOME"/bspwm/bspwmrc
else
    bspc wm -r
fi
