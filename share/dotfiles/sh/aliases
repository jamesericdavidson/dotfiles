# dotfiles  Configuration files and scripts for Debian GNU/Linux systems
# Copyright (C) 2020, 2021  James Davidson
#
# This file is part of dotfiles.
#
# dotfiles is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# dotfiles is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

source "$XDG_DATA_HOME"/dotfiles/sh/apt_aliases
source "$XDG_DATA_HOME"/dotfiles/sh/git_aliases

alias \
    cp='cp --interactive --verbose' \
    df='df --human-readable' \
    diff='diff --color' \
    du='du --human-readable' \
    fd=find \
    fdfind=find \
    ip='ip --color' \
    ln='ln --symbolic --verbose' \
    mkdir='mkdir --parents --verbose' \
    rg=grep \
    tp=trash-put \
    v='e -R'                # view (Vi read-only mode)

alias \
    ls='ls --color --group-directories-first' \
    la='ls --almost-all -l --human-readable' \
    ll='ls --human-readable -l' \
    lr='ls --recursive'

alias \
    se=sudoedit \
    si='sudo --login' \
    su='su --login'
