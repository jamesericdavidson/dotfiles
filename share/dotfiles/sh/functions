# dotfiles  Configuration files and scripts for Debian GNU/Linux systems
# Copyright (C) 2020, 2021  James Davidson
#
# This file is part of dotfiles.
#
# dotfiles is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# dotfiles is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

source "$XDG_DATA_HOME"/dotfiles/sh/git_functions

append()
{
    echo "$1" | tee --append "$2"
}

e()
{
    editor "$@" 2> /dev/null || "$EDITOR" "$@" 2> /dev/null || "$VISUAL" "$@"
}

find()
{
    env fdfind --hidden --no-ignore --ignore-case "$@" 2> /dev/null \
        || env find "$1" -iname "$2"
}

grep()
{
    env rg --hidden --ignore-case --no-ignore "$@" 2> /dev/null \
        || env grep --ignore-case --colour "$@"
}

p()
{
    pager "$@" 2> /dev/null || "$PAGER" "$@"
}

sa()
{
    alias | grep "$1"
}

sort()
{
    env sort "$1" --output="$1"
}
