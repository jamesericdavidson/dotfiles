"[GNU-Head Tiling wallpapers](https://www.gnu.org/graphics/wilgus-gnutiling.html)" by Kacper Wilgus is licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
