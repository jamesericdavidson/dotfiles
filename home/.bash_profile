# dotfiles  Configuration files and scripts for Debian GNU/Linux systems
# Copyright (C) 2020, 2021  James Davidson
#
# This file is part of dotfiles.
#
# dotfiles is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# dotfiles is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

if ! echo $SHELL | grep bash 1> /dev/null
then
    return 1
fi

export XDG_DATA_HOME="$HOME"/.local/share
source "$XDG_DATA_HOME"/dotfiles/sh/profile

export PATH="$PATH:/opt:$HOME/.local/bin"
export HISTFILE="$XDG_DATA_HOME"/bash/history

source "$HOME"/.bashrc
