# dotfiles  Configuration files and scripts for Debian GNU/Linux systems
# Copyright (C) 2020, 2021  James Davidson
#
# This file is part of dotfiles.
#
# dotfiles is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# dotfiles is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

source "$XDG_DATA_HOME"/dotfiles/sh/aliases
source "$XDG_DATA_HOME"/dotfiles/sh/functions

alias reload="source $HOME/.bashrc && source $HOME/.bash_profile"

shopt -s autocd dotglob histappend

COLOUR="$(tput setaf 2)"
RESET="$(tput sgr0)"
PS1='${COLOUR} \w \$${RESET} '
